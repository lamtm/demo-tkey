import Web3 from "web3";

export const convertWalletToArray = (objWallet)=>{
    let wallets = [];
    for (const [key] of Object.entries(objWallet)) {
        const newKey = String(key).toLocaleLowerCase();
        if(Web3.utils.isAddress(newKey) && !wallets.includes(newKey)){
            wallets = [...wallets, newKey];
        }
    }
    return wallets;
}

export const randomStr = (length) => {
    let result           = '';
    const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
