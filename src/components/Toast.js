import React from 'react'
import {ToastContainer, Toast} from 'react-bootstrap';

export default function Index({title, content, onClose, show, bg}) {
  
    return (
        <ToastContainer className="p-3" position='top-end' >
            <Toast onClose={onClose} show={show} delay={6000} autohide bg={bg} className='text-white'>
                <Toast.Header>
                    <strong className="me-auto">{title}</strong>
                </Toast.Header>
                <Toast.Body>{content}</Toast.Body>
            </Toast>
        </ToastContainer>
    );
  }
