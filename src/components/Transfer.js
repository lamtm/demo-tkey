import React, { useState } from 'react'
import {Card, Form, Button, Spinner} from 'react-bootstrap';
import { anikaToken, anikaContract } from 'configs/web3/index';

export default function Transfer({userRPC, setToast, onChangeBalance, balance}) {

  const [dataTransfer, setDataTransfer] = useState({
    address: '',
    amount: ''
  });

  const [loading, setLoading] = useState(false);

  const changeInput = (e)=>{
    setDataTransfer({...dataTransfer, [e.target.name]: e.target.value});
  }

  const submitTransfer = async (e)=>{
    e.preventDefault();
    if(!userRPC){
      setToast({
        title: 'Send Transaction',
        content: 'Please Import your account',
        show: true,
        bg: 'danger'
      });
      return;
    }
    if(loading) return false;
    if(balance < dataTransfer.amount){
      setToast({
        title: 'Send Transaction',
        content: 'Insufficient Balance',
        show: true,
        bg: 'danger'
      });
      return;
    }
    setLoading(true);
    try{
      const newAmount = dataTransfer.amount * `1e${anikaToken.symbols}`;
      const data = await anikaContract.connect(userRPC).transfer(dataTransfer.address, newAmount);
      setToast({
        title: 'Send Transaction',
        content: 'Send Transaction Successful!',
        show: true,
        bg: 'secondary'
      });
      onChangeBalance(dataTransfer.amount);
      console.log({data});
    }catch(e){
      setToast({
        title: 'Send Transaction',
        content: e?.message ?? 'Send Transaction Failed!',
        show: true,
        bg: 'danger'
      });
    }
    setLoading(false);
  }

  return (
    <div>
        <Card.Title>Transfer</Card.Title>
        <Form onSubmit={submitTransfer}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Address To</Form.Label>
            <Form.Control type="text" name="address" placeholder="Address..." onChange={changeInput} />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Amount (ETH)</Form.Label>
          <Form.Control type="number" name="amount" placeholder="Amount" onChange={changeInput} />
          </Form.Group>
          <Button variant="primary" type="submit" style={{width: '120px'}} disabled={loading}>
            {loading ?  <Spinner animation="border" variant="light" size="sm" /> : 'Submit'}
          </Button>
        </Form>
    </div>
  )
}
