import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import AnikanaWallet from "../../features/anikaWallet";
import Home from "features/home";
import Web3AuthDemo from "features/web3AuthDemo";
import MnemonicPhrase from "features/mnemonicPhrase";
import Web3AuthDemoSocial from "features/Web3AuthDemoSocial";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/web3-auth" element={<Web3AuthDemo />} />
          <Route path="/web3-auth-social" element={<Web3AuthDemoSocial />} />
          <Route path="/mnemonic" element={<MnemonicPhrase />} />
          <Route path="/anika-wallet" element={<AnikanaWallet />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
