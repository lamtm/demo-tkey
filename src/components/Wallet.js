import React, { useState } from 'react'
import {Card, Form, Button, Spinner} from 'react-bootstrap';
import { web3 } from 'configs/web3/index';

export default function Wallet({reloadWallet, setToast, listWallet, changeCurrentAccount}) {
  const [dataWallet, setDataWallet] = useState({
    private_key: ''
  });
  const [loading, setLoading] = useState(false);

  const changeInputWallet = (e)=>{
    setDataWallet({...dataWallet, [e.target.name]: e.target.value});
  }
      
  const submitWallet = async (e)=>{
    e.preventDefault();
    try{
      const newAccount = web3.eth.accounts.privateKeyToAccount(dataWallet.private_key);
      if(listWallet.includes(newAccount.address.toLocaleLowerCase())){
        throw Error("Wallet Has Been Imported!");
      }
      web3.eth.accounts.wallet.add(newAccount);
      reloadWallet();
      changeCurrentAccount(newAccount.address);
      setToast({
        title: 'Import Wallet',
        content: 'Import Wallet Successful!',
        show: true,
        bg: 'secondary'
      });
      setDataWallet({
        ...dataWallet,
        private_key: ''
      })
    }catch(e){
      setToast({
        title: 'Import Wallet',
        content: e?.message ?? 'Import Wallet Failed!',
        show: true,
        bg: 'danger'
      });
    }
  }
  
  const createWallet = ()=>{
    try{
      const newAccount = web3.eth.accounts.create();
      web3.eth.accounts.wallet.add(newAccount);
      reloadWallet();
      changeCurrentAccount(newAccount.address);
      setToast({
        title: 'Create Wallet',
        content: 'Create Wallet Successful!',
        show: true,
        bg: 'secondary'
      });
    }catch(e){
      setToast({
        title: 'Create Wallet',
        content: e?.message ?? 'Create Wallet Failed!',
        show: true,
        bg: 'danger'
      });
    }
  }

  return (
    <div>
        <Card.Title>Wallet</Card.Title>
        <Form onSubmit={submitWallet}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Private Key</Form.Label>
              <Form.Control type="text" name="private_key" placeholder="Private Key..." value={dataWallet.private_key} onChange={changeInputWallet} autoComplete="off"/>
            </Form.Group>
            <Button variant="primary" type="submit" style={{width: '120px'}} disabled={loading}>
            {loading ?  <Spinner animation="border" variant="light" size="sm" /> : 'Import'}
          </Button>
        </Form>
        <hr/>
        <div className='mt-3'>
          <Button variant="secondary" type="submit" style={{width: '130px'}} disabled={loading} onClick={createWallet}>
            {loading ?  <Spinner animation="border" variant="light" size="sm" /> : 'Create Wallet'}
          </Button>
        </div>
    </div>
  )
}
