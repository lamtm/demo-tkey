// import ChromeExtensionStorageModule from "@tkey/chrome-storage";
import WebStorageModule from "@tkey/web-storage";
import SecurityQuestionsModule from "@tkey/security-questions";
import ShareTransferModule from "@tkey/share-transfer";
// import SeedPhraseModule from "@tkey/seed-phrase";
import PrivateKeyModule from "@tkey/private-keys";
// import ShareSerializationModule from "@tkey/share-serialization";

// export const chromeStorageModule = new ChromeExtensionStorageModule();
export const webStorageModule = new WebStorageModule();
export const securityQuestionsModule = new SecurityQuestionsModule();
export const shareTransferModule = new ShareTransferModule();
// export const seedPhraseModule = new SeedPhraseModule();
export const privateKeyModule = new PrivateKeyModule();
// export const shareSerializationModule = new ShareSerializationModule();
