import TorusServiceProvider from "@tkey/service-provider-torus";

const customAuthArgs = {
  baseUrl: `${window.location.origin}/serviceworker`,
  enableLogging: true,
  network: "testnet",
};

export const serviceProvider = new TorusServiceProvider({ customAuthArgs });

export const loginVerifier = {
  // name: "Google",
  typeOfLogin: "google",
  clientId:
    "97726061816-2hkp3692nu5b404jlhpprqo2gv0u559l.apps.googleusercontent.com",
  verifier: "bap-google-testnet",
};
