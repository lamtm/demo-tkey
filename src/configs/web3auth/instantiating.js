import ThresholdKey from "@tkey/core";
import { serviceProvider } from "./serviceProvider";
import { storageLayer } from "./storageLayer";
import {
  securityQuestionsModule,
  shareTransferModule,
  webStorageModule,
} from "./storageModules";

export const tKey = new ThresholdKey({
  serviceProvider: serviceProvider,
  storageLayer,
  modules: {
    webStorage: webStorageModule,
    securityQuestions: securityQuestionsModule,
    shareTransfer: shareTransferModule,
  },
});
