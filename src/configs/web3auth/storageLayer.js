import TorusStorageLayer from "@tkey/storage-layer-torus";

export const storageLayer = new TorusStorageLayer({
  hostUrl: "https://metadata.tor.us",
});
