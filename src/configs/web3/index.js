import anikaAbi from './contracts/TokenANIKANA.json';
import { ethers } from 'ethers';
import {ANIKA} from 'configs/chains';
import Web3 from 'web3';

const MAX_GAS_LIMIT = 2000000000;
const options = {
    gasLimit: MAX_GAS_LIMIT
}

export const anikaToken = {
    address: '0x796C844Caee428F67651d9da0c8Ce17b48026a70',
    abi: anikaAbi,
    symbols: 9
}
export const anikaProvider = ethers.providers.getDefaultProvider(ANIKA.rpcUrls[0], options);
export const anikaContract = new ethers.Contract(anikaToken.address, anikaToken.abi, anikaProvider);
export const web3 = new Web3(new Web3.providers.HttpProvider(ANIKA.rpcUrls[0]));
