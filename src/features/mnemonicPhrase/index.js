import React, { useState } from "react";
import style from "./style.module.scss";
import { generateMnemonic, mnemonicToSeedSync, validateMnemonic } from "bip39";
import HDKey from "hdkey";

function MnemonicPhrase() {
  const [mnemonic, setMnemonic] = useState("");
  const [newMnemonic, setNewMnemonic] = useState("");
  const [message, setMessage] = useState("");
  const [derivationPath, setDerivationPath] = useState("m/44'/60'/0'/0");

  const deriveAccount = () => {
    if (!validateMnemonic(mnemonic)) {
      console.log("mnemonic invalid");
    } else if (!derivationPath) {
    } else {
      const bip39Seed = mnemonicToSeedSync(mnemonic).toString("hex");
      const hdKey = HDKey.fromMasterSeed(Buffer.from(bip39Seed, "hex"));

      const childHd = hdKey.derive(derivationPath);
      setMessage(
        "Private Key: " +
          childHd.privateKey.toString("hex") +
          "\nPublic Key: " +
          childHd.publicKey.toString("hex")
      );
    }
  };

  const generateAcount = () => {
    const bipMnemonic = generateMnemonic();
    setNewMnemonic(bipMnemonic);
  };

  return (
    <>
      <div className={style.container}>
        <div className="import">
          <h4>Mnemonic Phrase Import</h4>

          <div className="text-center">
            <input
              value={mnemonic}
              onChange={({ target }) => setMnemonic(target.value)}
              style={{ width: 800 }}
            />
          </div>
          <div className="text-center">BIP32 Derivation Path</div>
          <div className="text-center mb-1">
            <input
              type="text"
              className="text-center"
              value={derivationPath}
              onChange={({ target }) => setDerivationPath(target.value)}
            />
          </div>
          <div className="text-center mb-2">
            <button className="px-5" onClick={deriveAccount}>
              Derive
            </button>
          </div>
          <div className="text-center">
            <textarea
              style={{ width: 800 }}
              name="message"
              id="message"
              rows="5"
              value={message}
              readOnly
            />
          </div>
        </div>
        <div className="generate">
          <h4>Generate Mnemonic</h4>
          <div className="text-center">
            <textarea
              style={{ width: 600 }}
              className="text-center"
              name="new-mnemonic"
              id="new-mnemonic"
              rows="3"
              value={newMnemonic}
              readOnly
            />
          </div>
          <div className="text-center">
            <button onClick={generateAcount}>Generate</button>
          </div>
        </div>
      </div>
    </>
  );
}

export default MnemonicPhrase;
