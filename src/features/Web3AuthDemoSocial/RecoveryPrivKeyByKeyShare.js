import { tKey } from "configs/web3auth/instantiating";
import React, { useState } from "react";
import style from "./style.module.scss";
import { loginVerifier } from "configs/web3auth/serviceProvider";
import { web3 } from "configs/web3";

function RecoveryPrivKeyByKeyShare({ setCurrentAccount }) {
  const [answer, setAnswer] = useState();
  const [showInputAnswer, setShowInputAnswer] = useState(false);
  const triggerLogin = async () => {
    await tKey.serviceProvider.triggerLogin(loginVerifier);
  };

  const loginUsingLocalShare = async () => {
    try {
      await triggerLogin();
      await tKey.initialize();

      // console.log("Adding local webstorage share");
      const webStorageModule = tKey.modules["webStorage"];
      await webStorageModule.inputShareFromWebStorage();

      const indexes = tKey.getCurrentShareIndexes();
      console.log(indexes);
      console.log("Total number of available shares: " + indexes.length);
      const reconstructedKey = await tKey.reconstructKey();
      const privKey = reconstructedKey.privKey.toJSON();
      console.log("tKey.getKeyDetails(): ", tKey.getKeyDetails());
      console.log("privKey: " + privKey);
      showAccount(privKey);
    } catch (error) {
      console.error(error, "caught");
    }
  };

  const loginByPasswordShare = async () => {
    await triggerLogin();
    await tKey.initialize();
    setShowInputAnswer(true);
  };

  const confirmAnswer = async (answer) => {
    try {
      await tKey.modules.securityQuestions.inputShareFromSecurityQuestions(
        answer
      );
      setShowInputAnswer(false);
      const reconstructedKey = await tKey.reconstructKey();
      const privKey = reconstructedKey.privKey.toJSON();
      console.log("tKey Details: ", tKey.getKeyDetails());
      console.log("privKey: " + privKey);
      showAccount(privKey);
    } catch (error) {
      console.log(error);
    }
  };

  const showAccount = (privateKey) => {
    const account = web3.eth.accounts.privateKeyToAccount(privateKey);
    setCurrentAccount(account);
  };

  return (
    <div>
      <br />
      <div className="text-center">
        <h5>with 2/3 Share</h5>
        <button
          className={style.card}
          style={{ width: "60%" }}
          onClick={() => loginUsingLocalShare()}
        >
          <h5>Using Social login & Device Share</h5>
        </button>
      </div>
      <br />

      <div className="text-center">
        <button
          className={style.card}
          style={{ width: "60%" }}
          onClick={() => loginByPasswordShare()}
        >
          <h5>Using Social login & Security Questions</h5>
        </button>
        {showInputAnswer && (
          <div
            className={style.form_field}
            style={{ width: "60%", margin: "auto" }}
          >
            <div className={style.label}>What is your password ? </div>
            <div className={style.value}>
              <input
                value={answer}
                onChange={({ target }) => setAnswer(target.value)}
              />
              <button onClick={() => confirmAnswer(answer)}>confirm</button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default React.memo(RecoveryPrivKeyByKeyShare);
