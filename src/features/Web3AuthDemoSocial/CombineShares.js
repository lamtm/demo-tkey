import React, { useState } from "react";
import style from "./style.module.scss";

function CombineShares({ inputPrivKey, setInputPrivKey }) {
  const [share1, setShare1] = useState();
  const [share2, setShare2] = useState();
  const [combine, setCombine] = useState();

  const combineShares = () => {
    if (!share1 || !share2) {
      setCombine("Please 2 key share valid");
    }
    let comb = window.secrets.combine([share1, share2]);
    setCombine(comb);
    try {
    } catch (error) {
      setCombine(error.message);
    }
  };
  return (
    <>
      <h4>Combine Shares</h4>
      <div className={style.split_key_container}>
        <div className={style.form_field}>
          <div className={style.label}>share 1: </div>
          <div className={style.value}>
            <input
              value={inputPrivKey}
              onChange={({ target }) => setShare1(target.value)}
            />
          </div>
        </div>
        <div className={style.form_field}>
          <div className={style.label}>share 2: </div>
          <div className={style.value}>
            <input
              value={inputPrivKey}
              onChange={({ target }) => setShare2(target.value)}
            />
          </div>
        </div>
        <div className="text-center">
          <button className={style.card} onClick={combineShares}>
            Combine Shares
          </button>
        </div>
        <div className={style.form_field}>
          <div className={style.label}>&nbsp;</div>
          <div className={style.value}>
            <textarea
              name="priv-key-split"
              id="priv-key-split"
              rows="1"
              value={combine}
            />
          </div>
        </div>
      </div>
    </>
  );
}

export default React.memo(CombineShares);
