import React, { useEffect, useState } from "react";
import style from "./style.module.scss";
import { web3 } from "configs/web3/index";
import { CHAIN_NAMESPACES } from "@web3auth/base";
import { Web3Auth } from "@web3auth/web3auth";
import RPC from "../../utils/web3RPC";

function Web3AuthLogin({ currentAccount, setCurrentAccount }) {
  const [web3auth, setWeb3auth] = useState(null);
  const [provider, setProvider] = useState(null);
  const [showPrivKey, setShowPrivKey] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const init = async () => {
      try {
        setLoading(true);
        const web3auth = new Web3Auth({
          clientId: process.env.REACT_APP_CLIENT_ID, // get from https://dashboard.web3auth.io
          chainConfig: {
            chainNamespace: CHAIN_NAMESPACES.EIP155,
            chainId: process.env.REACT_APP_CHAIN_ID,
            rpcTarget: process.env.REACT_APP_RPC_TARGET,
          },
          enableLogging: false,
        });
        setWeb3auth(web3auth);

        await web3auth.initModal();
        if (web3auth.provider) {
          setProvider(web3auth.provider);
        }
        setLoading(false);
      } catch (error) {
        setLoading(false);
        console.error("try catch: ", error);
      }
    };

    init();
  }, []);

  useEffect(() => {
    if (provider) {
      getPrivateKey();
    }
  }, [provider]);

  const login = async () => {
    if (!web3auth) {
      console.log("web3auth not initialized yet");
      return;
    }
    const web3authProvider = await web3auth.connect();
    setProvider(web3authProvider);
    console.log("web3auth: ", web3auth);
  };

  const logout = async () => {
    if (!web3auth) {
      console.log("web3auth not initialized yet");
      return;
    }
    await web3auth.logout();
    setProvider(null);
    removeAccount();
  };

  const getPrivateKey = async () => {
    if (!provider) {
      console.log("provider not initialized yet");
      return;
    }
    const rpc = new RPC(provider);
    const privateKey = await rpc.getPrivateKey();
    showAccount(privateKey);
  };

  const createNewAccount = () => {
    try {
      const newAccount = web3.eth.accounts.create();
      setCurrentAccount(newAccount);
    } catch (error) {
      console.log("try catch: ", error);
    }
  };

  const showAccount = (privateKey) => {
    const account = web3.eth.accounts.privateKeyToAccount(privateKey);
    setCurrentAccount(account);
  };

  const removeAccount = () => {
    setCurrentAccount(null);
  };

  return (
    <>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <span className="h2">Account generate(web3auth)</span>
        {/* <button className={style.card} onClick={createNewAccount}>
          create New Account
        </button> */}
        {!loading &&
          (provider ? (
            <button className={style.card} onClick={logout}>
              logout
            </button>
          ) : (
            <button className={style.card} onClick={login}>
              Login/Register
            </button>
          ))}
      </div>
      <div className={style.account_container}>
        <div className={style.form_field}>
          <div className={style.label}>Address: </div>
          <div className={style.value}>
            <input value={currentAccount?.address || ""} readOnly />
          </div>
        </div>
        <div className={style.form_field}>
          <div className={style.label}>Private key: </div>
          <div className={style.value}>
            <input
              type={showPrivKey ? "text" : "password"}
              value={currentAccount?.privateKey || ""}
              disabled
            />
            <button onClick={() => setShowPrivKey((prev) => !prev)}>
              {showPrivKey ? "hide" : "show"}
            </button>
          </div>
        </div>
      </div>
    </>
  );
}

export default React.memo(Web3AuthLogin);
