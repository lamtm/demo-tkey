import { tKey } from "configs/web3auth/instantiating";
import React, { useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import CombineShares from "./CombineShares";
import CreateNewShare from "./CreateNewShare";
import RecoveryPrivKeyByKeyShare from "./RecoveryPrivKeyByKeyShare";
import SplitPrivateKey from "./SplitPrivateKey";
import style from "./style.module.scss";
import Web3AuthLogin from "./web3AuthLogin";

function Web3AuthDemoSocial() {
  const [inputPrivKey, setInputPrivKey] = useState();
  const [currentAccount, setCurrentAccount] = useState();

  React.useEffect(() => {
    tKey.serviceProvider.init({ skipSw: false });
  });

  return (
    <div className={style.container}>
      <Web3AuthLogin
        currentAccount={currentAccount}
        setCurrentAccount={setCurrentAccount}
      />
      <br />
      <br />
      <Tabs
        defaultActiveKey="Recovery"
        id="fill-tab-example"
        className="mb-3"
        fill
      >
        <Tab eventKey="Split/Combine Key" title={<h5>Split/Combine Key</h5>}>
          <SplitPrivateKey
            inputPrivKey={inputPrivKey}
            setInputPrivKey={setInputPrivKey}
          />
          <CombineShares />
        </Tab>
        <Tab eventKey="Share Key" title={<h5>Share Key</h5>}>
          <CreateNewShare />
        </Tab>
        <Tab eventKey="Recovery" title={<h5>Recovery Private Key</h5>}>
          <RecoveryPrivKeyByKeyShare setCurrentAccount={setCurrentAccount} />
        </Tab>
      </Tabs>
    </div>
  );
}

export default Web3AuthDemoSocial;
