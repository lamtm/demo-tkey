import React from "react";
import style from "./Home.module.scss";
import { useNavigate } from "react-router-dom";

function Home() {
  const navigate = useNavigate();
  return (
    <div className={style.container}>
      <h1 className={style.title}>
        <a target="_blank" href="http://web3auth.io/" rel="noreferrer">
          @tkey
        </a>
        & ReactJS Example
      </h1>

      <button className={style.card} onClick={() => navigate("/web3-auth")}>
        @TKey demo
      </button>

      <button
        className={style.card}
        onClick={() => navigate("/web3-auth-social")}
      >
        @TKey demo, Web3auth social login
      </button>

      <button className={style.card} onClick={() => navigate("/mnemonic")}>
        Mnemonic Phrase
      </button>

      <button className={style.card} onClick={() => navigate("/anika-wallet")}>
        anika-wallet demo
      </button>
    </div>
  );
}

export default Home;
