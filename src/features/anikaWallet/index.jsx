import Transfer from "components/Transfer";
import Wallet from "components/Wallet";
import { Eye, EyeSlash } from "configs/icons";
import { ethers } from "ethers";
import React, { useState } from "react";
import {
  Col,
  Container,
  Form,
  InputGroup,
  Row,
  Tab,
  Tabs,
  Toast,
} from "react-bootstrap";
import { convertWalletToArray } from "utils/handler";
import { anikaContract, anikaProvider, web3 } from "configs/web3/index";

// const PRIVATE_KEY = '';
// const userRPC = new ethers.Wallet(PRIVATE_KEY, anikaProvider);

// const myAccount = web3.eth.accounts.privateKeyToAccount(PRIVATE_KEY);
// web3.eth.accounts.wallet.add(myAccount);
const wallets = web3.eth.accounts.wallet;

const arrayWallet = convertWalletToArray(wallets);

function AnikanaWallet() {
  const [balance, setBalance] = useState();
  const [listWallet, setListWallet] = useState(arrayWallet);
  const [currentAccount, setCurrentAccount] = useState();
  const [togglePk, setTogglePk] = useState(false);
  const [toast, setToast] = useState({
    title: "",
    content: "",
    show: false,
  });

  React.useEffect(() => {
    loadBalance();
  }, [currentAccount]);

  const loadBalance = async () => {
    if (!currentAccount) return;
    const data = await anikaContract.balanceOf(currentAccount.address);
    setBalance(data.div(1e9).toString());
  };

  const onChangeBalance = (amount) => {
    setBalance(balance - amount);
  };

  const reloadWallet = (account) => {
    const wallets = web3.eth.accounts.wallet;
    const arrayWallet = convertWalletToArray(wallets);
    setListWallet(arrayWallet);
  };

  const onChangeAccount = (e) => {
    changeCurrentAccount(e.target.value);
  };

  const changeCurrentAccount = (address) => {
    const current = web3.eth.accounts.wallet[address];
    const myAccount = new ethers.Wallet(current, anikaProvider);
    setCurrentAccount(myAccount);
  };

  return (
    <Container>
      <h1 className="text-center text-dark mt-3">Anikana Wallet</h1>
      <Row className="align-items-center mb-3">
        <Col xl="2">
          <h5>Account:</h5>
        </Col>
        <Col xl="10">
          <Form.Select onChange={onChangeAccount} defaultValue={listWallet[0]}>
            {listWallet.map((i, idx) => (
              <option
                key={idx}
                value={i}
                selected={currentAccount.address.toLocaleLowerCase() === i}
              >
                {i}
              </option>
            ))}
          </Form.Select>
        </Col>
      </Row>
      <Row className="align-items-center mb-3">
        <Col xl="2">
          <h5>Private Key:</h5>
        </Col>
        <Col xl="10">
          <InputGroup className="mb-3">
            <Form.Control
              disabled={true}
              type={togglePk ? "text" : "password"}
              value={currentAccount?.privateKey ?? ""}
            />
            <InputGroup.Text onClick={() => setTogglePk(!togglePk)}>
              {togglePk ? <EyeSlash /> : <Eye />}
            </InputGroup.Text>
          </InputGroup>
        </Col>
      </Row>
      <Row className="align-items-center mb-3">
        <Col xl="2">
          <h5>Balance:</h5>
        </Col>
        <Col xl="10">
          <h5 className="text-danger">${balance}</h5>
        </Col>
      </Row>
      <Tabs
        defaultActiveKey="wallet"
        id="fill-tab-example"
        className="mb-3"
        fill
      >
        <Tab eventKey="wallet" title={<h5>Wallet</h5>}>
          <Wallet
            reloadWallet={reloadWallet}
            setToast={setToast}
            listWallet={listWallet}
            changeCurrentAccount={changeCurrentAccount}
          />
        </Tab>
        <Tab eventKey="transfer" title={<h5>Transfer</h5>}>
          <Transfer
            userRPC={currentAccount}
            loadBalance={loadBalance}
            setToast={setToast}
            balance={balance}
            onChangeBalance={onChangeBalance}
          />
        </Tab>
      </Tabs>
      <Toast onClose={() => setToast({ ...toast, show: false })} {...toast} />
    </Container>
  );
}

export default AnikanaWallet;
