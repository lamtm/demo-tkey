import React, { useState } from "react";
import style from "./style.module.scss";

const total = 3;
const threshold = 2;
function SplitPrivateKey({ inputPrivKey, setInputPrivKey }) {
  const [shares, setShares] = useState();
  const generateShares = () => {
    try {
      // var regEx = /[0-9a-fA-F]*/g;
      var keyToBeSplit = inputPrivKey?.replaceAll('"', "");
      if (keyToBeSplit?.substring(0, 2) === "0x") {
        keyToBeSplit = keyToBeSplit?.substring(2);
      }

      // if (keyToBeSplit && regEx.test(keyToBeSplit)) {
      if (keyToBeSplit) {
        var shares = window.secrets.share(keyToBeSplit, total, threshold);
        console.log("shares: ", shares);
        setShares(shares.join("\n"));
      } else {
        setShares("Please enter a valid hexadecimal number");
      }
    } catch (error) {
      setShares(error.message);
    }
  };

  return (
    <>
      <h4>Split the private key</h4>
      <div className={style.split_key_container}>
        <div className={style.form_field}>
          <div className={style.label}>Private key: </div>
          <div className={style.value}>
            <input
              value={inputPrivKey}
              onChange={({ target }) => setInputPrivKey(target.value)}
            />
          </div>
        </div>
        <div className="text-center">
          <button className={style.card} onClick={generateShares}>
            Split the private key
          </button>
          <p className="mb-2 fw-bold">
            Secret Sharing {threshold} out of {total}
          </p>
        </div>
        <div className={style.form_field}>
          <div className={style.label}>Private Key split into 3 shares: </div>
          <div className={style.value}>
            <textarea
              name="priv-key-split"
              id="priv-key-split"
              rows="4"
              value={shares}
            />
          </div>
        </div>
      </div>
    </>
  );
}

export default React.memo(SplitPrivateKey);
