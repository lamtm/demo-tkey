import BN from "bn.js";
import { tKey } from "configs/web3auth/instantiating";
import { loginVerifier } from "configs/web3auth/serviceProvider";
import { useState } from "react";
import style from "./style.module.scss";

function CreateNewShare() {
  const [answer, setAnswer] = useState();
  const [isDisabledAnswer, setIsDisabledAnswer] = useState(false);
  const [privKeyInput, setPrivKeyInput] = useState();
  const [isDisabledInputKey, setIsDisabledInputKey] = useState(false);
  const [isDisabledShareA, setIsDisabledShareA] = useState(false);
  const [isDisabledShareB, setIsDisabledShareB] = useState(false);
  const [isDisabledShareC, setIsDisabledShareC] = useState(false);

  const triggerLogin = async () => {
    await tKey.serviceProvider.triggerLogin(loginVerifier);
  };

  const importPrivKeyToTKey = async () => {
    var keyToBeSplit = privKeyInput.replaceAll('"', "");
    if (keyToBeSplit.substring(0, 2) === "0x") {
      keyToBeSplit = keyToBeSplit.substring(2);
    }

    const res = await tKey._initializeNewKey({
      initializeModules: false,
      importedKey: new BN(keyToBeSplit, "hex"),
      neverInitializeNewKey: true,
    });
    const privKey = res.privKey.toString("hex");

    setIsDisabledInputKey(true);
    setIsDisabledShareA(true);
    console.log("getKeyDetails: ", tKey.getKeyDetails());
    console.log("privKey: ", privKey);
  };

  const inittialNewTKey = async () => {
    try {
      if (!privKeyInput) {
        console.error("Please enter private key..");
        return;
      }
      await triggerLogin();
      await tKey.initialize();
      const totalShare = tKey.getKeyDetails().totalShares;

      if (totalShare > 2) {
        var result = window.confirm(
          "This asocial login stored Shares of other private key.\nDo you want overwrite Shares?"
        );

        if (result) {
          importPrivKeyToTKey();
        }
      } else {
        importPrivKeyToTKey();
      }
    } catch (error) {
      console.log("try catch: ", error);
      setPrivKeyInput(error.message);
    }
  };

  const ShareToDeviceShare = async () => {
    try {
      await tKey.modules.webStorage.getDeviceShare();
      console.log("Device Share is Exist");
      setIsDisabledShareB(true);
    } catch (error) {
      try {
        console.error("Generating a new share");
        const { newShareIndex, newShareStores } = await tKey.generateNewShare();

        await tKey.modules.webStorage.storeDeviceShare(
          newShareStores[newShareIndex.toString("hex")]
        );

        await tKey.syncLocalMetadataTransitions(); // push metadata to cloud
        setIsDisabledInputKey(true);
        setIsDisabledShareB(true);
        console.log("tKey Details: ", tKey.getKeyDetails());
      } catch (error) {
        console.log("try catch: ", error);
      }
    }
  };

  const shareToSecurityQuestions = async () => {
    try {
      await tKey.modules.securityQuestions.generateNewShareWithSecurityQuestions(
        answer,
        "What is your password ?"
      );
      await tKey.syncLocalMetadataTransitions(); // push metadata to cloud
      console.log("tKey Details: ", tKey.getKeyDetails());
      setIsDisabledAnswer(true);
      setIsDisabledShareC(true);
    } catch (error) {
      console.log("try catch: ", error);
    }
  };

  return (
    <div>
      <div className={style.form_field}>
        <div className={style.label}>Private key: </div>
        <div className={style.value}>
          <input
            value={privKeyInput}
            disabled={isDisabledInputKey}
            onChange={({ target }) => setPrivKeyInput(target.value)}
            placeholder="Enter private key..."
          />
        </div>
      </div>
      <br />
      <h5>Pair with your social login</h5>
      <div className={style.form_field}>
        <div className={style.label}></div>
        <div className={style.value}>
          <button
            className={style.card}
            onClick={() => inittialNewTKey()}
            disabled={isDisabledShareA}
          >
            Sign in on Google
          </button>
        </div>
      </div>
      <br />
      <h5>Allow device storage</h5>
      <div className={style.form_field}>
        <div className={style.label}></div>
        <div className={style.value}>
          <button
            className={style.card}
            onClick={() => ShareToDeviceShare()}
            disabled={isDisabledShareB}
          >
            Allow device
          </button>
        </div>
      </div>
      <br />
      <h5>Share to Security Questions</h5>
      <div className={style.form_field}>
        <div className={style.label}>What is your password ? </div>
        <div className={style.value}>
          <input
            value={answer}
            onChange={({ target }) => setAnswer(target.value)}
            disabled={isDisabledAnswer}
            placeholder="Enter answer..."
          />
        </div>
      </div>
      <div className={style.form_field}>
        <div className={style.label}></div>
        <div className={style.value}>
          <button
            disabled={isDisabledShareC}
            className={style.card}
            onClick={() => shareToSecurityQuestions()}
          >
            Share to Security Questions
          </button>
        </div>
      </div>
    </div>
  );
}

export default CreateNewShare;
