import React, { useState } from "react";
import style from "./style.module.scss";
import { web3 } from "configs/web3/index";

function CreateNewAccountWeb3({ currentAccount, setCurrentAccount }) {
  const [showPrivKey, setShowPrivKey] = useState(false);

  const createNewAccount = () => {
    try {
      const newAccount = web3.eth.accounts.create();
      console.log(newAccount);
      setCurrentAccount(newAccount);
    } catch (error) {
      console.log("try catch: ", error);
    }
  };

  return (
    <>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <span className="h2">Account generate(web3)</span>
        <button className={style.card} onClick={createNewAccount}>
          create New Account
        </button>
      </div>
      <div className={style.account_container}>
        <div className={style.form_field}>
          <div className={style.label}>Address: </div>
          <div className={style.value}>
            <input value={currentAccount?.address} readOnly />
          </div>
        </div>
        <div className={style.form_field}>
          <div className={style.label}>Private key: </div>
          <div className={style.value}>
            <input
              type={showPrivKey ? "text" : "password"}
              value={currentAccount?.privateKey}
              disabled
            />
            <button onClick={() => setShowPrivKey((prev) => !prev)}>
              {showPrivKey ? "hide" : "show"}
            </button>
          </div>
        </div>
      </div>
    </>
  );
}

export default React.memo(CreateNewAccountWeb3);
