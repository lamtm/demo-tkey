import { tKey } from "configs/web3auth/instantiating";
import React, { useState } from "react";
import style from "./style.module.scss";

function CreateNewTKey({ setInputPrivKey }) {
  const [privKey, setPrivKey] = useState();
  const requestLogin = async () => {
    await tKey.serviceProvider.triggerLogin({
      typeOfLogin: "google",
      verifier: "web3auth-testnet-verifier",
      clientId:
        "134678854652-vnm7amoq0p23kkpkfviveul9rb26rmgn.apps.googleusercontent.com",
    });
  };
  const inittialNewkey = async () => {
    try {
      await requestLogin();
      await tKey.initialize();
      const res = await tKey._initializeNewKey({ initializeModules: true });
      const privKey = res.privKey.toString("hex");
      setPrivKey(privKey);
      setInputPrivKey(privKey);
      console.log("privKey hex: ", privKey);
      console.log("getKeyDetails: ", tKey.getKeyDetails(), tKey);
    } catch (error) {
      console.log("try catch: ", error);
      setPrivKey(error.message);
    }
  };

  return (
    <>
      <h2>Private key generate</h2>
      <div className={style.form_field}>
        <div className={style.label}>Private key: </div>
        <div className={style.value}>
          <input value={privKey} disabled />
        </div>
      </div>
      <div className="text-center">
        <button className={style.card} onClick={() => inittialNewkey()}>
          create private key
        </button>
      </div>
    </>
  );
}

export default React.memo(CreateNewTKey);
