import { tKey } from "configs/web3auth/instantiating";
import React, { useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import CombineShares from "./CombineShares";
import CreateNewAccountWeb3 from "./CreateNewAccountWeb3";
import CreateNewShare from "./CreateNewShare";
import LoginByKeyShare from "./LoginByKeyShare";
import SplitPrivateKey from "./SplitPrivateKey";
import style from "./style.module.scss";

function Web3AuthDemo() {
  const [inputPrivKey, setInputPrivKey] = useState();
  const [currentAccount, setCurrentAccount] = useState();

  React.useEffect(() => {
    tKey.serviceProvider.init({ skipSw: false });
  });

  return (
    <div className={style.container}>
      <CreateNewAccountWeb3
        setInputPrivKey={setInputPrivKey}
        currentAccount={currentAccount}
        setCurrentAccount={setCurrentAccount}
      />
      <br />
      <br />
      <Tabs
        defaultActiveKey="Login"
        id="fill-tab-example"
        className="mb-3"
        fill
      >
        <Tab eventKey="Split/Combine Key" title={<h5>Split/Combine Key</h5>}>
          <SplitPrivateKey
            inputPrivKey={inputPrivKey}
            setInputPrivKey={setInputPrivKey}
          />
          <CombineShares />
        </Tab>
        <Tab eventKey="Share Key" title={<h5>Share Key</h5>}>
          <CreateNewShare />
        </Tab>
        <Tab eventKey="Login" title={<h5>Login</h5>}>
          <LoginByKeyShare setCurrentAccount={setCurrentAccount} />
        </Tab>
      </Tabs>
    </div>
  );
}

export default Web3AuthDemo;
