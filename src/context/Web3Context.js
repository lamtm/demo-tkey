import React, { createContext, useContext, useState, useEffect } from 'react';
import {anikaProvider, anikaContract, anikaToken, web3} from './web3/index';

export const InitWeb3Context = createContext();

export default function Web3Context() {
    return useContext(InitWeb3Context);
}

const wallets = web3.eth.accounts.wallet;

export const Web3ContextProvider = ({ children }) => {

    const listWallet = useState(wallets);

    return <InitWeb3Context.Provider value={{listWallet}}>
        {children}
    </InitWeb3Context.Provider>
}
